import random
import arcade
import os

PLAYER_SCALING = 0.15
SPRITE_SCALING = 0.5

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Lab 9 Game"

# How many pixels to keep as a minimum margin between the character
# and the edge of the screen.
VIEWPORT_MARGIN = 150

MOVEMENT_SPEED = 5


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height, title):
        """
        Initializer
        """
        super().__init__(width, height, title)

        # Set the working directory (where we expect to find files) to the same
        # directory this .py file is in. You can leave this out of your own
        # code, but it is needed to easily run the examples using "python -m"
        # as mentioned at the top of this program.
        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)

        # Sprite lists
        self.player_list = None
        self.coin_list = None
        self.gold_coin_list = None

        # Set up the player
        self.score = 0
        self.player_sprite = None
        self.wall_list = None
        self.physics_engine = None
        self.view_bottom = 0
        self.view_left = 0

        # sounds
        self.silver_coin_sound = arcade.load_sound("audio/coin1.wav")
        self.gold_coin_sound = arcade.load_sound("audio/coin9.wav")

    def setup(self):
        """ Set up the game and initialize the variables. """

        # Sprite lists
        self.player_list = arcade.SpriteList()
        self.wall_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()
        self.gold_coin_list = arcade.SpriteList()

        # Set up the player
        self.score = 0
        self.player_sprite = arcade.Sprite("img/monkey.png", PLAYER_SCALING)
        self.player_sprite.center_x = 500
        self.player_sprite.center_y = 900
        self.player_list.append(self.player_sprite)

        # ground wall
        for x in range(32, 1000, 64):
            wall = arcade.Sprite("img/grass.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 32
            self.wall_list.append(wall)

        # left wall
        for y in range(32, 992, 64):
            wall = arcade.Sprite("img/stoneCenter.png", SPRITE_SCALING)
            wall.center_x = 32
            wall.center_y = y + 64
            self.wall_list.append(wall)

        # right wall
        for y in range(32, 992, 64):
            wall = arcade.Sprite("img/stoneCenter.png", SPRITE_SCALING)
            wall.center_x = 992
            wall.center_y = y + 64
            self.wall_list.append(wall)

        # top wall
        for x in range(32, 1000, 64):
            wall = arcade.Sprite("img/sandCenter.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 1000
            self.wall_list.append(wall)

        # randomized walls r1
        for x in range(96, 992, 64):
            if random.randint(1, 2) is 2:
                wall = arcade.Sprite("img/planetCenter.png", SPRITE_SCALING)
                wall.center_x = x
                wall.center_y = 224
                self.wall_list.append(wall)

        # randomized walls r2
        for x in range(96, 992, 64):
            if random.randint(1, 2) is 2:
                wall = arcade.Sprite("img/planetCenter.png", SPRITE_SCALING)
                wall.center_x = x
                wall.center_y = 352
                self.wall_list.append(wall)

        # randomized walls r3
        for x in range(96, 992, 64):
            if random.randint(1, 2) is 2:
                wall = arcade.Sprite("img/planetCenter.png", SPRITE_SCALING)
                wall.center_x = x
                wall.center_y = 544
                self.wall_list.append(wall)

        # randomized walls r4/ not random
        for x in range(96, 600, 64):
            wall = arcade.Sprite("img/planetCenter.png", SPRITE_SCALING)
            wall.center_x = x
            wall.center_y = 672
            self.wall_list.append(wall)

        # coins placed
        for x in range(96, 992, 64):
            if random.randint(1, 2) is 2:
                coin = arcade.Sprite("img/coinSilver.png", SPRITE_SCALING)
                coin.center_x = x
                coin.center_y = 128
                self.coin_list.append(coin)

        # coins placed
        for x in range(96, 992, 64):
            if random.randint(1, 2) is 2:
                coin = arcade.Sprite("img/coinSilver.png", SPRITE_SCALING)
                coin.center_x = x
                coin.center_y = 412
                self.coin_list.append(coin)

        # gold coins placed
        for x in range(96, 992, 64):
            if random.randint(1, 3) is 2:
                gold_coin = arcade.Sprite("img/coinGold.png", SPRITE_SCALING)
                gold_coin.center_x = x
                gold_coin.center_y = 604
                self.gold_coin_list.append(gold_coin)

        # gold coins placed
        for x in range(96, 992, 64):
            if random.randint(1, 3) is 2:
                gold_coin = arcade.Sprite("img/coinGold.png", SPRITE_SCALING)
                gold_coin.center_x = x
                gold_coin.center_y = 476
                self.gold_coin_list.append(gold_coin)

        self.physics_engine = arcade.PhysicsEngineSimple(self.player_sprite, self.wall_list)

        # Set the background color
        arcade.set_background_color(arcade.color.AMAZON)

        # Set the viewport boundaries
        # These numbers set where we have 'scrolled' to.
        self.view_left = 0
        self.view_bottom = 0

    def on_draw(self):
        """
        Render the screen.
        """

        # This command has to happen before we start drawing
        arcade.start_render()

        # Draw all the sprites.
        self.coin_list.draw()
        self.gold_coin_list.draw()
        self.wall_list.draw()
        self.player_list.draw()

        score_output = f"Score {self.score}"
        arcade.draw_text(score_output, SCREEN_WIDTH/2, 80, arcade.color.WHITE, 25)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player_sprite.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player_sprite.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player_sprite.change_x = 0

    def update(self, delta_time):
        """ Movement and game logic """

        # Call update on all sprites (The sprites don't do much in this
        # example though.)
        self.physics_engine.update()

        coin_hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.coin_list)
        gc_coin_hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.gold_coin_list)


        # --- Manage Scrolling ---

        # Track if we need to change the viewport

        changed = False

        # Scroll left
        left_bndry = self.view_left + VIEWPORT_MARGIN
        if self.player_sprite.left < left_bndry:
            self.view_left -= left_bndry - self.player_sprite.left
            changed = True

        # Scroll right
        right_bndry = self.view_left + SCREEN_WIDTH - VIEWPORT_MARGIN
        if self.player_sprite.right > right_bndry:
            self.view_left += self.player_sprite.right - right_bndry
            changed = True

        # Scroll up
        top_bndry = self.view_bottom + SCREEN_HEIGHT - VIEWPORT_MARGIN
        if self.player_sprite.top > top_bndry:
            self.view_bottom += self.player_sprite.top - top_bndry
            changed = True

        # Scroll down
        bottom_bndry = self.view_bottom + VIEWPORT_MARGIN
        if self.player_sprite.bottom < bottom_bndry:
            self.view_bottom -= bottom_bndry - self.player_sprite.bottom
            changed = True

        if changed:
            arcade.set_viewport(self.view_left,
                                SCREEN_WIDTH + self.view_left,
                                self.view_bottom,
                                SCREEN_HEIGHT + self.view_bottom)

        for coin in coin_hit_list:
            coin.kill()
            self.score += 1
            arcade.play_sound(self.silver_coin_sound)
            # add sound effects

        for gold_coin in gc_coin_hit_list:
            gold_coin.kill()
            self.score += 3
            arcade.play_sound(self.gold_coin_sound)
            # add sound effects


def main():
    """ Main method """
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
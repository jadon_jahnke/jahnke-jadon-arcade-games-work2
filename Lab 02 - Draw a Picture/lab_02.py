import arcade

# jadn vaporizer pod list
point_list = ((305, 385),
              (315, 395),
              (335, 395),
              (345, 385),
              (335, 375),
              (315, 375),
              (305, 385),
              )

# jadn graphic pod list
graphic_point_list = ((545, 385),
                      (555, 395),
                      (575, 395),
                      (585, 385),
                      (575, 375),
                      (555, 375),
                      (545, 385),
                      )

# opens window
arcade.open_window(800, 600, "Lab 2 drawing")

# background
arcade.set_background_color(arcade.color.AERO_BLUE)

# starts the drawing
arcade.start_render()

# jadn vaporizer
arcade.draw_lrtb_rectangle_filled(300, 350, 385, 150, arcade.color.ALABAMA_CRIMSON)
arcade.draw_lrtb_rectangle_filled(300, 350, 430, 385, arcade.color.BLACK)
arcade.draw_circle_filled(325, 350, 2, arcade.color.BATTLESHIP_GREY)
arcade.draw_polygon_filled(point_list, arcade.color.BATTLESHIP_GREY)

# jadn pods pack
arcade.draw_lrtb_rectangle_outline(400, 600, 450, 150, arcade.color.BLACK, 5)
arcade.draw_lrtb_rectangle_filled(400, 600, 450, 150, arcade.color.WHITE)

# jadn graphic
arcade.draw_lrtb_rectangle_filled(540, 590, 385, 250, arcade.color.ALABAMA_CRIMSON)
arcade.draw_lrtb_rectangle_filled(540, 590, 430, 385, arcade.color.BLACK)
arcade.draw_circle_filled(565, 350, 2, arcade.color.BATTLESHIP_GREY)
arcade.draw_polygon_filled(graphic_point_list, arcade.color.BATTLESHIP_GREY)

# Labels
arcade.draw_text("JADN", 410, 410, arcade.color.BLACK, 25)

arcade.draw_text("JADN Device", 410, 390, arcade.color.BLACK, 8)
arcade.draw_text("USB Charging Dock", 410, 380, arcade.color.BLACK, 8)
arcade.draw_text("LIMITED EDITION", 410, 350, arcade.color.BLACK, 12)
arcade.draw_text("The Alternative", 410, 270, arcade.color.BLACK, 8)
arcade.draw_text("For Adult Smokers", 410, 260, arcade.color.BLACK, 8)

# warning label
arcade.draw_lrtb_rectangle_outline(400, 600, 250, 150, arcade.color.BLACK, 3)
arcade.draw_text("Warning:", 460, 230, arcade.color.BLACK, 15)
arcade.draw_text("Warning:", 460, 230, arcade.color.BLACK, 15)
arcade.draw_text("This product contains ", 420, 215, arcade.color.BLACK, 12)
arcade.draw_text("This product contains ", 420, 215, arcade.color.BLACK, 12)
arcade.draw_text("nicotine. Nicotine is an", 415, 197, arcade.color.BLACK, 12)
arcade.draw_text("nicotine. Nicotine is an", 415, 197, arcade.color.BLACK, 12)
arcade.draw_text("addictive chemical.", 425, 179, arcade.color.BLACK, 12)
arcade.draw_text("addictive chemical.", 425, 179, arcade.color.BLACK, 12)

arcade.finish_render()

# runs our picture
arcade.run()

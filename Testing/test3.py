# Put your code below:
# Put your code below:
def print_triangle(row):
    for i in range(row):
        for k in range(row - i - 1):
            print(".", end=" ")
        for i in range(row - k - 1):
            print(i, end=" ")

        print()


# Here are some example calls to test your code. Don't change the code below:
print_triangle(5)
print()
print_triangle(3)
print()
print_triangle(9)

# Here are some example calls to test your code. Don't change the code below:
print_triangle(5)
print()
print_triangle(3)
print()
print_triangle(9)
class Cat():

    def __init__(self):
        self.name = ""
        self.color = ""
        self.weight = 0

    def meow(self):
        print("Meow")


def main():

    my_cat = Cat()
    my_cat.name = "Jack"

    print("my cats name is:", my_cat.name)

main()
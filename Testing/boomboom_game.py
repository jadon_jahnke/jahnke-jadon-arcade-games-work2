""" Sprite Sample Program """

import arcade
import random

# --- Constants ---
SPRITE_SCALING_BOX = 2
SPRITE_SCALING_PLAYER = 2

SPRITE_SCALING_COIN = 1
SPRITE_SCALING_SNAKE = 1.5

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# Bullets
SPRITE_SCALING_BULLET = 0.8
BULLET_SPEED = 15

COIN_COUNT = 15
SNAKE_COUNT = 25

MOVEMENT_SPEED = 5

VIEWPORT_MARGIN = 40


class Snake(arcade.Sprite):

    def __init__(self, filename, sprite_scaling):

        super().__init__(filename, sprite_scaling)
        self.change_x = 0
        self.change_y = 0

    def update(self):

        # Move the snake
        self.center_x += self.change_x
        self.center_y += self.change_y

        # If we are out-of-bounds, then 'bounce'
        if self.left < 200:
            self.change_x *= -1

        if self.right > 700:
            self.change_x *= -1

        if self.bottom < 100:
            self.change_y *= -1

        if self.top > SCREEN_HEIGHT:
            self.change_y *= -1


class Coin(arcade.Sprite):

    def __init__(self, filename, sprite_scaling):

        super().__init__(filename, sprite_scaling)
        self.change_x = 0
        self.change_y = 0

    def update(self):

        # Move the coin
        self.center_x += self.change_x
        self.center_y += self.change_y

        # If we are out-of-bounds, then 'bounce'
        if self.left < 0:
            self.change_x *= -1

        if self.right > SCREEN_WIDTH:
            self.change_x *= -1

        if self.bottom < 100:
            self.change_y *= -1

        if self.top > SCREEN_HEIGHT:
            self.change_y *= -1


class MyGame(arcade.Window):
    """ This class represents the main window of the game. """

    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Boom Boom game")

        self.player_list = None
        self.wall_list = None
        self.snake_list = None
        self.coin_list = None
        self.bullet_list = None
        self.player_bullet_list = None
        self.enemy_bullet_list = None


        # Set up the player
        self.player_sprite = None
        self.score = 0
        self.count = 1

        self.frame_count = 0

        self.set_mouse_visible(False)

        # This variable holds our simple "physics engine"
        self.physics_engine = None


    def setup(self):
        # Set the background color
        arcade.set_background_color(arcade.color.BLACK_LEATHER_JACKET)

        self.player_list = arcade.SpriteList()
        self.wall_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()
        self.snake_list = arcade.SpriteList()
        self.bullet_list = arcade.SpriteList()
        self.enemy_bullet_list = arcade.SpriteList()
        self.player_bullet_list = arcade.SpriteList()

        # Reset the score
        self.score = 0

        self.view_bottom = 0
        self.view_left = 0

        # Create the player
        self.player_sprite = arcade.Sprite("img/ball_player1.png", SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = SCREEN_WIDTH/2
        self.player_sprite.center_y = SCREEN_HEIGHT/2
        self.player_list.append(self.player_sprite)

        for i in range(COIN_COUNT):

            # Create the coin instance
            # gem image from kenney.nl
            coin = Coin("img/boomboom_star0.png", SPRITE_SCALING_COIN)

            # Position the coin
            coin.center_x = random.randrange(50, SCREEN_WIDTH)
            coin.center_y = random.randrange(105, SCREEN_HEIGHT)
            coin.change_x = random.randrange(-3, 4)
            coin.change_y = random.randrange(-3, 4)

            # Add the coin to the lists
            self.coin_list.append(coin)



            # Create the snake instance
            # snake image from kenney.nl
        snake = Snake("img/triangle_enemy1.png", SPRITE_SCALING_SNAKE)

        # Position the snake
        snake.center_x = SCREEN_WIDTH/2
        snake.center_y = SCREEN_HEIGHT
        snake.change_x = random.randrange(1, 4)
        snake.change_y = 0

        # Add the snake to the lists
        self.snake_list.append(snake)

        self.enemy_bullet = arcade.Sprite("img/enemy_bullet.png")
        self.enemy_bullet.center_x = snake.change_x
        self.enemy_bullet.center_y = snake.change_y
        self.enemy_bullet.angle = 0
        self.enemy_bullet.change_x = BULLET_SPEED
        self.enemy_bullet_list.append(self.enemy_bullet)

        # create a wall
        # bottom wall
        for x in range(32, 800, 64):
            wall = arcade.Sprite("img/rectangle_wall.png", SPRITE_SCALING_BOX)
            wall.center_x = x
            wall.center_y = 32
            self.wall_list.append(wall)
        # top wall
        for x in range(32, 832, 64):
            wall = arcade.Sprite("img/rectangle_wall.png", SPRITE_SCALING_BOX)
            wall.center_x = x
            wall.center_y = SCREEN_WIDTH
            self.wall_list.append(wall)
        # left wall
        for Y in range(32, 800, 64):
            wall = arcade.Sprite("img/rectangle_wall.png", SPRITE_SCALING_BOX)
            wall.center_x = 32
            wall.center_y = Y
            self.wall_list.append(wall)

        for Y in range(32, 800, 64):
            wall = arcade.Sprite("img/rectangle_wall.png", SPRITE_SCALING_BOX)
            wall.center_x = SCREEN_WIDTH
            wall.center_y = Y
            self.wall_list.append(wall)

        self.physics_engine = arcade.PhysicsEngineSimple(self.player_sprite, self.wall_list)

        self.view_left = 0
        self.view_bottom = 0

    def on_draw(self):
        arcade.start_render()

        arcade.start_render()
        self.wall_list.draw()
        self.player_list.draw()
        self.snake_list.draw()
        self.coin_list.draw()
        self.player_bullet_list.draw()
        self.bullet_list.draw()
        self.enemy_bullet_list.draw()

        output = f"Score: {self.score}"
        arcade.draw_text(output, 370, 100, arcade.color.WHITE, 20)


    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player_sprite.change_x = MOVEMENT_SPEED
        elif key == arcade.key.SPACE:
            player_bullet = arcade.Sprite("img/boomboom_star0.png")
            player_bullet.center_x = self.player_sprite.center_x
            player_bullet.center_y = self.player_sprite.center_y
            player_bullet.angle = 0
            player_bullet.change_x = BULLET_SPEED
            self.player_bullet_list.append(player_bullet)

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player_sprite.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player_sprite.change_x = 0

    def update(self, delta_time):

        self.frame_count += 1
        self.coin_list.update()
        self.snake_list.update()
        self.bullet_list.update()
        self.player_bullet_list.update()

        # Generate a list of all sprites that collided with the player.
        hit_list = arcade.check_for_collision_with_list(self.player_sprite,
                                                        self.coin_list)
        snake_hit_list = arcade.check_for_collision_with_list(self.player_sprite,
                                                              self.snake_list)

        # Loop through each colliding sprite, remove it, and add to the score.
        for coin in hit_list:
            coin.kill()
            self.count += 1
            self.score += 1

            if len(self.coin_list) == 0:
                # need to get game to end
                pass

        for snake in snake_hit_list:
            snake.kill()
            self.score -= 1

        self.physics_engine.update()

        changed = False

        # Scroll left
        left_bndry = self.view_left + VIEWPORT_MARGIN
        if self.player_sprite.left < left_bndry:
            self.view_left -= left_bndry - self.player_sprite.left
            changed = True

        # Scroll right
        right_bndry = self.view_left + SCREEN_WIDTH - VIEWPORT_MARGIN
        if self.player_sprite.right > right_bndry:
            self.view_left += self.player_sprite.right - right_bndry
            changed = True

        # Scroll up
        top_bndry = self.view_bottom + SCREEN_HEIGHT - VIEWPORT_MARGIN
        if self.player_sprite.top > top_bndry:
            self.view_bottom += self.player_sprite.top - top_bndry
            changed = True

        # Scroll down
        bottom_bndry = self.view_bottom + VIEWPORT_MARGIN
        if self.player_sprite.bottom < bottom_bndry:
            self.view_bottom -= bottom_bndry - self.player_sprite.bottom
            changed = True

        self.view_left = int(self.view_left)
        self.view_bottom = int(self.view_bottom)

        if changed:
            arcade.set_viewport(self.view_left,
                                SCREEN_WIDTH + self.view_left,
                                self.view_bottom,
                                SCREEN_HEIGHT + self.view_bottom)

def main():
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
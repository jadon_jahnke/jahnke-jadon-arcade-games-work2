# work with context manager like loops or if statements


def main():
    my_file = open("super_villains.txt")
    my_list = []

    for line in my_file:
        line = line.strip()
        my_list.append(line)

    my_file.close()

    key = "Morgiana the Shrew"

    # ---linear search---
    current_list_position = 0
    while current_list_position < len(my_list) and my_list[current_list_position] != key:
        current_list_position += 1

    # PRINT RESULTS
    if my_list[current_list_position] < len(my_list):
        print("Found it!")
    else:
        print("did not find it")

    if current_list_position >= len(my_list):
        print("Did not find it!")

main()

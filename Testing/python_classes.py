class Character():
    """
    This is a class that represents a character.
    """
    def __init__(self):
        self.name = ""
        self.outfit = "Green"
        self.max_hit_points = 0
        self.current_hit_points = 0
        self.max_speed = 0
        self.armor_amount = 0

import random


def main():
    print("""Welcome to Camel!
You have stolen a camel to make your way across the great Mobi desert.
The natives want their camel back and are chasing you down! Survive your
desert trek and out run the natives.\n""")

    miles_traveled = 0
    thirst = 0
    camel_tiredness = 0
    native_traveled = -20
    canteen_drinks = 3
    done = False

    while done is False:
        print("""A. Drink from your canteen.
B. Ahead moderate speed.
C. Ahead full speed.
D. Stop for the night.
E. Status check.
Q. Quit.""")

        user_input = input("What is your choice? ")

        if user_input.upper() == "Q":
            done = True
            print("Game Ended")
        elif user_input.upper() == "E":
            print("Miles traveled: ", miles_traveled)
            print("Drinks in canteen: ", canteen_drinks)
            print("The natives are ", native_traveled - miles_traveled, " miles behind you!\n")
        elif user_input.upper() == "D":
            camel_tiredness = 0
            native_traveled += random.randint(7, 14)
            print("The camel is happy.")
        elif user_input.upper() == "C":
            miles_traveled += random.randint(10, 21)
            print("You have traveled: ", miles_traveled)
            thirst += 1
            camel_tiredness += random.randint(1, 3)
            native_traveled += random.randint(7, 14)

        elif user_input.upper() == "B":
            miles_traveled += random.randint(5, 13)
            print("You have traveled: ", miles_traveled)
            thirst += 1
            camel_tiredness += 1
            native_traveled += random.randint(7, 14)
        elif user_input.upper() == "A":

            if canteen_drinks > 0:
                canteen_drinks -= 1
                thirst = 0
            else:
                print("No Drinks left!")

        if thirst > 4:
            if thirst > 6:
                print("You died of thirst!")
                done = True
            else:
                print("You are thirsty!")

        elif camel_tiredness > 5:
            if camel_tiredness > 8:
                print("your camel died!")
                done = True
            else:
                print("Your camel is tired!")

        elif miles_traveled <= 200:
            if native_traveled >= miles_traveled - 15:
                if native_traveled >= miles_traveled:
                    print("The natives have caught you!")
                    done = True
                else:
                    print("The natives are getting close!")

        elif miles_traveled >= 200:
            print("You've evaded the natives and escaped!")
            done = True


main()

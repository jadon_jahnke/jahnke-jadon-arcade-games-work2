def main():
    room_list = []
    # bedroom
    room = ["This is your bedroom, there is a door to the east.", None, 1, None, None]
    room_list.append(room)
    # south Hall
    room = ["You enter the south hall, You can go East this is your brothers room."
            "Or follow the hall north.", 5, 2, None, 0]
    room_list.append(room)
    # brothers bedroom
    room = ["This is your brother's room, smells like disappointment..., ", None, None, None, 1]
    room_list.append(room)
    # kitchen
    room = ["You enter the kitchen, there is a door to the east.", None, 4, None, None]
    room_list.append(room)
    # Living room
    room = ["You enter the living room, there is a door to the east and west.", None, 5, None, 3]
    room_list.append(room)
    # north Hall
    room = ["You enter the north hall, You can go west to the living room,"
            "south to the south hall, or east to your parents room."
            " ", None, 6, 1, 4]
    room_list.append(room)
    # parents bedroom
    room = ["You enter your parents bedroom. You see the delicious brownies.", None, None, None, 5]
    room_list.append(room)

    current_room = 0

    done = False

    while done is False:
        print()
        print(room_list[current_room][0])

        user_input = str(input("What will you do? Enter x to QUIT: \n"))

        if user_input.lower() == "north":
            next_room = room_list[current_room][1]
            if user_input is None:
                print("You can't go this way.")
                continue
            else:
                current_room = next_room

        elif user_input.lower() == "east":
            next_room = room_list[current_room][2]

            if user_input is None:
                print("You can't go this way.")
                continue
            else:
                current_room = next_room

        elif user_input.lower() == "south":
            next_room = room_list[current_room][3]

            if user_input is None:
                print("You can't go this way.")
                continue
            else:
                current_room = next_room

        elif user_input.lower() == "west":
            next_room = room_list[current_room][4]

            if user_input is None:
                print("You can't go this way.")
                continue
            else:
                current_room = next_room

        elif user_input.lower() == "x":
            done = True

        else:
            print("You can't do this.")


main()

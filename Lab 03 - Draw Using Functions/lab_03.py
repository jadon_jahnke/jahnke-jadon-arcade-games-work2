import arcade

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 700


def draw_face(x, y):

    # Body
    arcade.draw_ellipse_filled(x, y - 100, 300, 400, arcade.color.BLACK_LEATHER_JACKET)

    # head and lips
    arcade.draw_circle_filled(x + 220, y + 150, 200, arcade.color.FRENCH_PINK)
    arcade.draw_ellipse_filled(x + 220, y + 80, 80, 20, arcade.color.ALABAMA_CRIMSON)
    arcade.draw_line(x + 140, y + 80, x + 300, y + 80, arcade.color.BLACK)

    # ears
    arcade.draw_ellipse_filled(x + 30, y + 190, 20, 60, arcade.color.FRENCH_PINK)
    arcade.draw_ellipse_outline(x + 30, y + 190, 10, 50, arcade.color.BLACK)
    arcade.draw_ellipse_filled(x + 410, y + 190, 20, 60, arcade.color.FRENCH_PINK)
    arcade.draw_ellipse_outline(x + 410, y + 190, 10, 50, arcade.color.BLACK)

    # eyes
    arcade.draw_ellipse_filled(x + 160, y + 200, 40, 20, arcade.color.WHITE)
    arcade.draw_ellipse_filled(x + 280, y + 200, 40, 20, arcade.color.WHITE)
    arcade.draw_ellipse_filled(x + 160, y + 200, 20, 20, arcade.color.BLACK)
    arcade.draw_ellipse_filled(x + 280, y + 200, 20, 20, arcade.color.BLACK)


def draw_juul(x, y):
    """Draw a juul"""

    point_list = ((5 + x, 85 + y),
                  (15 + x, 95 + y),
                  (35 + x, 95 + y),
                  (45 + x, 85 + y),
                  (35 + x, 75 + y),
                  (15 + x, 75 + y),
                  (5 + x, 85 + y),
                  )

    # juul
    arcade.draw_lrtb_rectangle_filled(x, 50 + x, 85 + y, y - 150, arcade.color.ALABAMA_CRIMSON)
    arcade.draw_lrtb_rectangle_filled(x, 50 + x, 130 + y, 85 + y, arcade.color.BLACK)
    arcade.draw_circle_filled(25 + x, 50 + y, 2, arcade.color.BATTLESHIP_GREY)
    arcade.draw_polygon_filled(point_list, arcade.color.BATTLESHIP_GREY)


def black_shades(x, y):
    arcade.draw_line(x, y, x + 220, y, arcade.color.BLACK, 5)
    arcade.draw_parabola_filled(x + 25, y - 50, x + 75, 50, arcade.color.BLACK, 180)
    arcade.draw_parabola_filled(x + 145, y - 50, x + 195, 50, arcade.color.BLACK, 180)


def airpods(x, y):
    arcade.draw_lrtb_rectangle_filled(x, x + 10, y + 70, y, arcade.color.WHITE_SMOKE)
    arcade.draw_circle_filled(x + 10, y + 70, 12, arcade.color.WHITE_SMOKE)
    # arcade.draw_circle_filled(x , y + 70, 12, arcade.color.WHITE_SMOKE)


def on_draw(delta_time):
    """ Draw everything """
    arcade.start_render()

    draw_face(300, 300)
    black_shades(410, on_draw.black_shades)
    airpods(on_draw.airpods_r, 380)
    airpods(on_draw.airpods_l, 380)

    # This gives an error but runs how I expected.
    draw_juul(500, on_draw.draw_juul)
    if on_draw.draw_juul < 250:
        on_draw.draw_juul += 1
        on_draw.black_shades -= 1
        on_draw.airpods_r += 1
        on_draw.airpods_l -= 1


on_draw.draw_juul = 50
on_draw.black_shades = 725
on_draw.airpods_r = 120
on_draw.airpods_l = 910


def main():
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 3")
    arcade.set_background_color(arcade.color.DARK_BLUE)

    # Finish and run
    arcade.schedule(on_draw, 1 / 60)
    arcade.run()


# Call the main function to get the program started.
main()

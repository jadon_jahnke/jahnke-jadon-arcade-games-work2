""" Lab 7 - User Control """

import arcade

# --- Constants ---
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SPEED = 10


def draw_face(x, y):

    # Body
    arcade.draw_ellipse_filled(x, y - 100, 300, 400, arcade.color.BLACK_LEATHER_JACKET)

    # head and lips
    arcade.draw_circle_filled(x + 220, y + 150, 200, arcade.color.FRENCH_PINK)
    arcade.draw_ellipse_filled(x + 220, y + 80, 80, 20, arcade.color.ALABAMA_CRIMSON)
    arcade.draw_line(x + 140, y + 80, x + 300, y + 80, arcade.color.BLACK)

    # ears
    arcade.draw_ellipse_filled(x + 30, y + 190, 20, 60, arcade.color.FRENCH_PINK)
    arcade.draw_ellipse_outline(x + 30, y + 190, 10, 50, arcade.color.BLACK)
    arcade.draw_ellipse_filled(x + 410, y + 190, 20, 60, arcade.color.FRENCH_PINK)
    arcade.draw_ellipse_outline(x + 410, y + 190, 10, 50, arcade.color.BLACK)

    # eyes
    arcade.draw_ellipse_filled(x + 160, y + 200, 40, 20, arcade.color.WHITE)
    arcade.draw_ellipse_filled(x + 280, y + 200, 40, 20, arcade.color.WHITE)
    arcade.draw_ellipse_filled(x + 160, y + 200, 20, 20, arcade.color.BLACK)
    arcade.draw_ellipse_filled(x + 280, y + 200, 20, 20, arcade.color.BLACK)


class Juul:
    def __init__(self, position_x, position_y, radius, style):

        self.position_x = position_x
        self.position_y = position_y
        self.radius = radius
        self.style = style

    def draw_juul(self):
        """Draw a juul"""

        point_list = ((5 + self.position_x, 85 + self.position_y),
                      (15 + self.position_x, 95 + self.position_y),
                      (35 + self.position_x, 95 + self.position_y),
                      (45 + self.position_x, 85 + self.position_y),
                      (35 + self.position_x, 75 + self.position_y),
                      (15 + self.position_x, 75 + self.position_y),
                      (5 + self.position_x, 85 + self.position_y),
                      )

        # juul
        arcade.draw_lrtb_rectangle_filled(self.position_x, 50 + self.position_x, 85 + self.position_y, self.position_y - 150, arcade.color.ALABAMA_CRIMSON)
        arcade.draw_lrtb_rectangle_filled(self.position_x, 50 + self.position_x, 130 + self.position_y, 85 + self.position_y, arcade.color.BLACK)
        arcade.draw_circle_filled(25 + self.position_x, 50 + self.position_y, 2, arcade.color.BATTLESHIP_GREY)
        arcade.draw_polygon_filled(point_list, arcade.color.BATTLESHIP_GREY)


    def update(self):
        bump_sound = arcade.load_sound("audio/knifeSlice.ogg")

        if self.position_x > SCREEN_WIDTH - self.radius:
            self.position_x = SCREEN_WIDTH - self.radius
            arcade.play_sound(bump_sound)

        if self.position_x < self.radius:
            self.position_x = self.radius
            arcade.play_sound(bump_sound)

        if self.position_y < self.radius:
            self.position_y = self.radius
            arcade.play_sound(bump_sound)

        if self.position_y > SCREEN_HEIGHT - self.radius:
            self.position_y = SCREEN_HEIGHT - self.radius
            arcade.play_sound(bump_sound)


class Shades:
    def __init__(self, position_x, position_y,change_x, change_y, radius, style):

        self.position_x = position_x
        self.position_y = position_y
        self.change_x = change_x
        self.change_y = change_y
        self.radius = radius
        self.style = style

    def draw_shades(self):
        """Draw shades"""
        arcade.draw_line(self.position_x, self.position_y, self.position_x + 220, self.position_y, arcade.color.BLACK, 5)
        arcade.draw_parabola_filled(self.position_x + 25, self.position_y - 50, self.position_x + 75, 50, arcade.color.BLACK, 180)
        arcade.draw_parabola_filled(self.position_x + 145, self.position_y - 50, self.position_x + 195, 50, arcade.color.BLACK, 180)

    def update(self):
        coolguy_sound = arcade.load_sound("audio/handleCoins.ogg")

        self.position_x += self.change_x
        self.position_y += self.change_y

        if self.position_x > SCREEN_WIDTH - self.radius:
            self.position_x = SCREEN_WIDTH - self.radius
            arcade.play_sound(coolguy_sound)
            self.position_x = self

        if self.position_x < self.radius:
            self.position_x = self.radius
            arcade.play_sound(coolguy_sound)
            self.change_x = 0

        if self.position_y < self.radius:
            self.position_y = self.radius
            arcade.play_sound(coolguy_sound)
            self.change_y = 0

        if self.position_y > SCREEN_HEIGHT - self.radius:
            self.position_y = SCREEN_HEIGHT - self.radius
            arcade.play_sound(coolguy_sound)
            self.change_y = 0


class MyGame(arcade.Window):
    """ Our Custom Window Class"""

    def __init__(self):
        """ Initializer """

        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 7 - User Control")

        self.set_mouse_visible(False)

        self.juul = Juul(200, 200, 20, arcade.color.RED_DEVIL)
        self.shades = Shades(100, 100, 0, 0, 20, arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        draw_face(300, 300)
        self.juul.draw_juul()
        self.shades.draw_shades()

    def update(self, delta_time):
        self.juul.update()
        self.shades.update()

    def on_mouse_motion(self, x, y, dx, dy):
        self.juul.position_x = x
        self.juul.position_y = y

    def on_key_press(self, key, modifiers):
        juul_sound = arcade.load_sound("audio/metalClick.ogg")

        if key == arcade.key.UP:
            self.shades.change_y = SPEED
        if key == arcade.key.DOWN:
            self.shades.change_y = -SPEED
        if key == arcade.key.LEFT:
            self.shades.change_x = -SPEED
        if key == arcade.key.RIGHT:
            self.shades.change_x = SPEED
        if key == arcade.key.SPACE:
            arcade.play_sound(juul_sound)

    def on_key_release(self, key, modifiers):
        if key == arcade.key.UP:
            self.shades.change_y = 0
        if key == arcade.key.DOWN:
            self.shades.change_y = 0
        if key == arcade.key.LEFT:
            self.shades.change_x = 0
        if key == arcade.key.RIGHT:
            self.shades.change_x = 0


def main():
    window = MyGame()
    arcade.run()


main()
